import { expect } from "chai";

export function checkStatusCode(response, statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500) {
    expect(response.statusCode, `Status Code should be ${statusCode}`).to.equal(statusCode);
}

export function checkResponseTime(response, maxResponseTime: number = 2000) {
    expect(response.timings.phases.total, `Response time should be less than ${maxResponseTime}ms`).to.be.lessThan(maxResponseTime);
}

export function checkValidationSchema(response, schema) {
    expect(response.body, `Verify validation schema`).to.be.jsonSchema(schema); 
}

export function checkResponseLength(response, item) {
    expect(response.body.length, `Response body should have more than ${item} item`).to.be.greaterThan(item); 
}

export function checkEmailAuth(response, email) {
    expect(response.body.user.email, `Verify user email`).to.be.equal(email); 
}

export function checkUserIdAuth(response, id) {
    expect(response.body.user.id, `Verify user id`).to.be.equal(id); 
}

export function checkEmail(response, email) {
    expect(response.body.email, `Verify user email`).to.be.equal(email); 
}

export function checkUserId(response, id) {
    expect(response.body.id, `Verify user id`).to.be.equal(id); 
}

export function checkAuthorId(response, id) {
    expect(response.body.author.id, `Verify user id`).to.be.equal(id); 
}

export function checkUserName(response, userName) {
    expect(response.body.userName, `Verify user name`).to.be.equal(userName); 
}
