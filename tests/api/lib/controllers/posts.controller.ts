import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;

export class PostsController {
    
    async newPost(authorIdValue: Number, previewImageValue: string, bodyValue: string, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body({
                authorId: authorIdValue,              
                previewImage: previewImageValue,
                body: bodyValue,
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts`)
            .send();
        return response;
    }

    async likePost(entityIdValue: Number, isLikeValue: boolean, userIdValue: Number, accessToken: string) {
        const response = await new ApiRequest() 
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body({
                entityId: entityIdValue,              
                isLike: isLikeValue,
                userId: userIdValue,
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async commentPost(authorIdValue: Number, postIdValue: Number, bodyValue: string, accessToken: string) {
        const response = await new ApiRequest() 
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Comments`)
            .body({
                authorId: authorIdValue,              
                postId: postIdValue,
                body: bodyValue,
            })
            .bearerToken(accessToken)
            .send();
        return response;
    } 
}