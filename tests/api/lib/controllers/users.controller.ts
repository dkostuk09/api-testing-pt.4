import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;

export class UsersController {
    async getAllUsers() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Users`)
            .send();
        return response;
    }

    async getUserById(id) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Users/${id}`)
            .send();
        return response;
    }

    async getUserFromToken(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Users/fromToken`)
            .bearerToken(accessToken)
            .send();
        return response;
    }
   
    async updateUser(idValue: Number, avatarValue: string, emailValue: string, userNameValue: string, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("PUT")
            .url(`api/Users`)
            .body({
                id: idValue,              
                avatar: avatarValue,
                email: emailValue,                        
                userName: userNameValue,
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }
}