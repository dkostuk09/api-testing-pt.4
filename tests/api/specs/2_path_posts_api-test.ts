import { 
    checkStatusCode, 
    checkResponseTime, 
    checkValidationSchema, 
    checkResponseLength,
    checkEmailAuth,
    checkUserIdAuth,
    checkAuthorId
} from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import { UsersController } from "../lib/controllers/users.controller";

const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

const auth = new AuthController();
const posts = new PostsController();
const users = new UsersController();

describe("Second path", () => {
    let userId: number;
    let email: string;
    let accessToken: string;
    let postId: number;

    let password: string = "password";

    before(`Registration`, async () => {
        let response = await auth.register(0, "avatar", "daria.MyEmail@gmail.com" , "user", password);

        checkStatusCode(response, 201);
        checkResponseTime(response, 1000);
        checkValidationSchema(response, schemas.schema_register);
        
        userId = response.body.user.id;
        email = response.body.user.email;  
    });

    it(`Authorization with invalid credentials`, async () => {
        let invalidPassword: string = "invalidPassword"

        let response = await auth.login(email, invalidPassword);   

        checkStatusCode(response, 401);
        checkResponseTime(response, 1000);
    });

    it(`Authorization`, async () => {
        let response = await auth.login(email, password);   

        checkEmailAuth(response, email);
        checkUserIdAuth(response, userId);
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkValidationSchema(response, schemas.schema_register);

        accessToken = response.body.token.accessToken.token;
    });

    it(`Placement the post`, async () => {
        let response = await posts.newPost(userId, "image", "hello", accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);  
        checkAuthorId(response, userId);
        checkValidationSchema(response, schemas.schema_newPost); 
        
        postId = response.body.id;
    });

    it(`Get all posts`, async () => {
        let response = await posts.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkResponseLength(response, 1);
    });
          
    it(`Like my post`, async () => {
        let response = await posts.likePost(postId, true, userId, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);       
    });

    it(`Comment my post`, async () => {
        let response = await posts.commentPost(userId, postId, "cool", accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkAuthorId(response, userId)
        checkValidationSchema(response, schemas.schema_comment);         
    });

    it(`Comment post with invalid id`, async () => {
        let invalidPostId: Number = 959595959;
   
        let response = await posts.commentPost(userId, invalidPostId, "cool", accessToken);

        checkStatusCode(response, 500);
        checkResponseTime(response, 1000);    
    });

    after(`Delete user`, async () => {
        let response = await auth.deleteUser(userId, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response, 1000);
    });   
});