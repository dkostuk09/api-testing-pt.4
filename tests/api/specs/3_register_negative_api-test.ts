import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";

const auth = new AuthController();

describe("Registration using invalid data", () => {
    let invalidDataForRegistration = [
        {email: " ", userName: " ", password: " "},
        {email: "user@gmail.com", userName: "user", password: "U"},
        {email: "user@gmail.com", userName: "user", password: "1234567890UserUser"},
        {email: "user@gmail.com", userName: "us", password: "User"},
        {email: "@gmail.com", userName: "user", password: "User"},       
        {email: "user", userName: "user", password: "User"},
    ];

    invalidDataForRegistration.forEach((data) => {
        it(`Registration with data: email - "${data.email}", user name - "${data.userName}", password - "${data.password}"`, async () => {
            let response = await auth.register(0, "avatar", data.email, data.userName, data.password);
    
            checkStatusCode(response, 400);
            checkResponseTime(response, 1000);
        });
    });
});