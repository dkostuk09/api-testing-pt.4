import { 
    checkStatusCode, 
    checkResponseTime, 
    checkValidationSchema, 
    checkResponseLength,
    checkEmailAuth,
    checkUserIdAuth,
    checkEmail,
    checkUserId,
    checkUserName
} from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";

const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

const auth = new AuthController();
const users = new UsersController();

describe("First path", () => {
    let userId: number;
    let email: string;
    let accessToken: string;

    let password: string = "password";

    before(`Registration`, async () => {
        let response = await auth.register(0, "avatar", "daria.myEmail@gmail.com" , "user", password);

        checkStatusCode(response, 201);
        checkResponseTime(response, 1000);
        checkValidationSchema(response, schemas.schema_register)
        
        userId = response.body.user.id;
        email = response.body.user.email;  
    });

    it(`Get all users`, async () => {
        let response = await users.getAllUsers();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkResponseLength(response, 1);       
    });

    it(`Authorization`, async () => {
        let response = await auth.login(email, password);   

        checkEmailAuth(response, email);
        checkUserIdAuth(response, userId);
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkValidationSchema(response, schemas.schema_register);
        
        accessToken = response.body.token.accessToken.token;
    });
    
    it(`Get user from token`, async () => {
        let response = await users.getUserFromToken(accessToken);

        checkEmail(response, email);
        checkUserId(response, userId);
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkValidationSchema(response, schemas.schema_userData);
    });

    let newEmail: string = "myNewEmail@gmail.com";

    it(`Update user`, async () => {
        let response = await users.updateUser(userId, "newAvatar", newEmail, "newUser", accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response, 1000);
    });

    it(`Get updated user from token`, async () => {
        let response = await users.getUserFromToken(accessToken);

        checkEmail(response, newEmail);
        checkUserId(response, userId);
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);  
        checkValidationSchema(response, schemas.schema_userData);
    });

    it(`Get user by id`, async () => {
        let response = await users.getUserById(userId);

        checkEmail(response, newEmail);
        checkUserId(response, userId);
        checkUserName(response, "newUser"); 
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkValidationSchema(response, schemas.schema_userData);
    });

    after(`Delete user`, async () => {
        let response = await auth.deleteUser(userId, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response, 1000);
    }); 
});